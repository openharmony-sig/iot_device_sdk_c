# OpenHarmony环境构建指导

## 目录

- [0 约定](#0)
- [1 OpenHarmony添加子系统](#1)
  -  [1.1 OpenHarmony 3.1 添加子系统编译构建](#1.1)
  -  [1.2 OpenHarmony 3.2 添加子系统编译构建](#1.2)
  -  [1.3 OpenHarmony 4.0 添加子系统编译构建](#1.3)
  -  [1.4 OpenHarmony 4.1 & 5.0添加组件编译构建](#1.4)
- [2 编译依赖的动态库](#2)
  -  [2.1 编译第三方动态库](#2.1)
  -  [2.2 其它三方库依赖](#2.2)
- [3 配置华为云接入参数并编译主程序](#3)
- [4 验证对接华为云](#4)

<h1 id="0">0 约定</h1>

本指导以OpenHarmony 3.1.4和rk3568为例，阐述如何用子系统方式使用该SDK。构建过程中需要复制获取对应库的头文件，请参考[README_CN.md](README_CN.md)的第3章节。

为方便说明，我们将OpenHarmony代码路径存储到环境变量`MY_OHOS_DIR`中，并在之后的步骤中使用该环境变量：

```bash
cd 您的OpenHarmony源码路径
MY_OHOS_DIR=$(pwd)
```

<h1 id="1">1 OpenHarmony添加子系统</h1>

<h2 id="1.1">1.1 OpenHarmony 3.1 添加子系统编译构建</h2>

在文件`$MY_OHOS_DIR/build/subsystem_config.json`中添加如下构建脚本（注意前面要添加一个逗号，):

```json
"iot_device_sdk_c" : {
    "path": "third_party/iot_device_sdk_c",
    "name": "iot_device_sdk_c"
}
```

具体如下图所示:
![](doc/doc_cn/ohos_build/add_subsys_in_build.png)
在产品配置文件中添加上述子系统，其子系统名称与上述文件中添加内容对应，即`iot_device_sdk_c:iot_device_sdk_c`。

此处以rk3568为例，配置文件位于`$MY_OHOS_DIR/productdefine/common/products/rk3568.json`,具体如下图所示：

![](doc/doc_cn/ohos_build/add_subsys_in_product.png)

<h2 id="1.2">1.2 OpenHarmony 3.2 添加子系统编译构建</h2>

[OpenHarmony3.2Release](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-release.md)版本与OpenHarmony3.1Release略有不同，可查看官网[API差异报告](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/api-diff/v3.2-Release)。同时代码目录结构也略有变动，如device、productdefine、vendor等等，固OpenHarmony适配文档需要针对版本做如下改动。下载源码后可以执行如下命令进行编译RK3568 ，./build.sh --product-name rk3568，则会在源码根目录下生成ohos_config.json文件。具体如下图所示。

![](doc/doc_cn/ohos_build/ohos_config_json.png)

重点关注图中的第1和2两处，第1处表示子系统配置文件，可以理解为子系统要在该文件中进行声明，第2处为产品配置文件，可以理解为最终要编译的系统文件。针对第1处与OpenHarmony3.1 一致，在`$MY_OHOS_DIR/build/subsystem_config.json`中添加构件子系统iot_device_sdk_c，代码如下（注意前面要添加一个逗号，):

```json
"iot_device_sdk_c" : {
    "path": "third_party/iot_device_sdk_c",
    "name": "iot_device_sdk_c"
}
```

之后在上图中第二处添加子系统名称，这里由于只有一个组件，因此为了方便，子系统名称与组件名称保持一致了。打开源码根目录下在`$MY_OHOS_DIR/vendor/hihope/rk3568/config.json`文件中，添加要编译的子系统iot_device_sdk_c，添加组件iot_device_sdk_c。代码如下（注意前面要添加一个逗号，）

```json
{
  "subsystem": "iot_device_sdk_c",
  "components": [
    {
      "component": "iot_device_sdk_c",
      "features": []
    }
  ]
}
```

具体如下图所示：

<img src="doc/doc_cn/ohos_build/config_json.png" style="zoom:135%;" />

<h2 id="1.3">1.3 OpenHarmony 4.0 添加子系统编译构建</h2>

当前的OpenHarmony4.0-Release当前版本不是很稳定，如果源码直接遇到编译错误可以参考[issue中我的解决方案](https://gitee.com/openharmony/build/issues/I8G7Z9#note_22715644)，适配过程同3.2一致，如果遇到如下图中问题

![](doc/doc_cn/ohos_build/oh4_build_error.png)

原因是OH代码加入了编译检查，临时措施是把需要编译检查的文件放到白名单里面，请在`$MY_OHOS_DIR/build/compile_standard_whitelist.json`中添加如下语句：

```json
"third_party/iot_device_sdk_c/bundle.json"
```

具体如下图所示：

![](doc/doc_cn/ohos_build/compile_standard_whitelist_json.png)

由于4.0版本与3.2版本略有不同，需要在源码根目录在`$MY_OHOS_DIR/build/common/bundle.json`中添加如下代码，其中"third_party": ["openssl"]为必填项。

```json
"deps": {
  "components": [
     "libcrypto_static",
     "ssl_source"
   ],

   "third_party": [
     "openssl"
   ]
},
```

具体如下图所示：

![](doc/doc_cn/ohos_build/bundle_json.png)

![](doc/doc_cn/ohos_build/attention.png)**若报错pthread_cancel未定义，当前版本仍存在报错情况，采用如下解决方案**。（后面版本可能不报错，欢迎其他解决方案提issue进行交流）

**解决方案1：**如果**不使用异常检测功能**，则将`$MY_OHOS_DIR/third_party/iot_device_sdk_c/src/service/detect_anomaly/detect_anomaly.c`中的语句`(void)pthread_cancel(g_reportMemoryTaskId);`注释掉。

**解决方案2：**目前如果**使用异常检测功能**，将函数 `static void *Detect_ReportDetectionInfoEntry(void *args)` 和函数`static void Detect_ReportDetectionInfo(void)`替换为如下语句：

```c
static pthread_cond_t g_taskCond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t g_taskMutex = PTHREAD_MUTEX_INITIALIZER;
static int g_reportMemoryTaskRunning = -1;
static void *Detect_ReportDetectionInfoEntry(void *args)
{
    while (1) {
        pthread_mutex_lock(&g_taskMutex);
        if (g_reportMemoryTaskRunning != 1) {
            pthread_cond_wait(&g_taskCond, &g_taskMutex);
        }
        pthread_mutex_unlock(&g_taskMutex);
        size_t i;
        for (i = 0; i < sizeof(g_mTasks) / sizeof(g_mTasks[0]); i++) {
            if (g_mTasks[i].check && g_mTasks[i].getReport) {
                cJSON *content = cJSON_CreateObject();
                g_mTasks[i].getReport(content);
                Detect_ReportModuleInfo(g_mTasks[i].reportType, content);
                cJSON_Delete(content);
            }
        }
        usleep(DETECT_REPORT_FREQUENCY * 1000 * 1000);
    }
    return args; // for pthread interface
}

static void Detect_ReportDetectionInfo(void)
{
    int securityConfig = 0;

    size_t i;
    for (i = 0; i < sizeof(g_mTasks) / sizeof(g_mTasks[0]); i++) {
        securityConfig = securityConfig || g_mTasks[i].newCheck;
        if (g_mTasks[i].newCheck && !g_mTasks[i].check && g_mTasks[i].init) {
            g_mTasks[i].init();
        }
        if (!g_mTasks[i].newCheck && g_mTasks[i].check && g_mTasks[i].destroy) {
            g_mTasks[i].destroy();
        }
        g_mTasks[i].check = g_mTasks[i].newCheck;
    }
    if (securityConfig == SWITCH_ON) {
        if (g_reportMemoryTaskRunning == -1) {
            PrintfLog(EN_LOG_LEVEL_DEBUG, "start dectection thread\n");
            pthread_cond_init(&g_taskCond, NULL);
            pthread_mutex_init(&g_taskMutex, NULL);
            pthread_create(&g_reportMemoryTaskId, NULL, Detect_ReportDetectionInfoEntry, NULL);
        }
        pthread_mutex_lock(&g_taskMutex);
        g_reportMemoryTaskRunning = 1;
        pthread_mutex_unlock(&g_taskMutex);
        pthread_cond_signal(&g_taskCond); //wake up thread 1
    } else {
        PrintfLog(EN_LOG_LEVEL_DEBUG, "stop dectection thread\n");
        g_reportMemoryTaskRunning = -1;
        pthread_mutex_unlock(&g_taskMutex);
    }
}
```

<h2 id="1.4">1.4 OpenHarmony 4.1 & 5.0添加组件编译构建</h2>

（1）与OpenHarmony4.0相比，OpenHarmony 4.1 & 5.0中对于子系统与组件进行了更加严格的关系校验，详见文件`developtools/syscap_codec/tools/syscap_collector.py`。而在rk3568的编译配置文件`vendor/hihope/rk3568/config.json`中已经有thirdparty子系统的编译，因此将iot_device_sdk_c作为一个组件添加到该子系统下参与编译，则无需修改`$MY_OHOS_DIR/build/subsystem_config.json`文件，主要需要修改如下文件即可：`iot_device_sdk_c/bundle.json`中的subsystem名称修改为`thirdparty`，具体如下图所示**（若为存量代码，则需要自己手动修改，若初次下载代码，则可以略过这个步骤）**。

<img src="doc/doc_cn/ohos_build/41devicesdk_bundle.json.png" style="zoom:%100;" />

（2）需要将`vendor/hihope/rk3568/config.json`中需要将组件信息添加到thirdparty子系统下，注意前面要补充逗号（`,`）。

```json
{
    "component": "iot_device_sdk_c",
    "features": []
}
```

具体如下图所示：

<img src="doc/doc_cn/ohos_build/41component.png" title=" style=&quot;zoom:%10;" style="zoom:120%;" />

（3）把需要编译检查的文件放到白名单里面，请在`$MY_OHOS_DIR/build/compile_standard_whitelist.json`中添加如下语句：

```json
"third_party/iot_device_sdk_c/bundle.json"
```

具体如下图所示：![](doc/doc_cn/ohos_build/compile_standard_whitelist_json.png)

（4）与OpenHarmony 4.0目录架构略有不同，在`$MY_OHOS_DIR/build/bundle.json`文件中将OpenSSL源码以及crypto加密库添加到组件中参与编译，其中"third_party": ["openssl"]为必填项，源码如下。

```json
"deps": {
  "components": [
     "libcrypto_static",
     "ssl_source"
   ],

   "third_party": [
     "openssl"
   ]
},
```

具体如下图所示：

![](doc/doc_cn/ohos_build/bundle_json5.png)

<h1 id="2">2 编译依赖的动态库</h1>

此sdk主要依赖于`libboundscheck.so`,`libpaho-mqtt3as.so`,`libssh.so`,`libnopoll.so`
这些动态库，其间接依赖于`libz.so`,`libssl.so`, `libcrypto.so`。

<h2 id="2.1">2.1 编译第三方动态库</h2>

对于`libboundscheck.so`，`libpaho-mqtt3as.so`，`libssh.so`，`libnopoll.so`，
这些库对应的源码没有在OpenHarmony中集成，如果您的开发板中没有这些动态库，您需要自行下载并编译。

执行以下命令拉取对应源码：

```bash
cd $MY_OHOS_DIR/third_party/iot_device_sdk_c
mkdir third_party
cd third_party
git clone https://gitee.com/Janisa/huawei_secure_c.git \
  && git clone -b v1.3.12 --single-branch https://github.com/eclipse/paho.mqtt.c.git  \
  && git clone -b libssh-0.9.6 --single-branch https://git.libssh.org/projects/libssh.git  \
  && git clone -b 0.4.8 --single-branch https://github.com/ASPLes/nopoll.git
```

分别在`$MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/huawei_secure_c`，`$MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/paho.mqtt.c`，
`$MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/libssh`，
`$MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/nopoll`
文件夹中放入或替换BUILD.gn文件，内容如下：

```
# 放入 $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/huawei_secure_c/BUILD.gn
#
# Copyright (c) [2020] Huawei Technologies Co.,Ltd.All rights reserved.
#
# OpenArkCompiler is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#
#     http://license.coscl.org.cn/MulanPSL
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
# FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v1 for more details.
#
import("//build/ohos.gni")

src_libHWSecureC = [
  "src/vsprintf_s.c",
  "src/wmemmove_s.c",
  "src/strncat_s.c",
  "src/vsnprintf_s.c",
  "src/fwscanf_s.c",
  "src/scanf_s.c",
  "src/strcat_s.c",
  "src/sscanf_s.c",
  "src/secureprintoutput_w.c",
  "src/wmemcpy_s.c",
  "src/wcsncat_s.c",
  "src/secureprintoutput_a.c",
  "src/secureinput_w.c",
  "src/memcpy_s.c",
  "src/fscanf_s.c",
  "src/vswscanf_s.c",
  "src/secureinput_a.c",
  "src/sprintf_s.c",
  "src/memmove_s.c",
  "src/swscanf_s.c",
  "src/snprintf_s.c",
  "src/vscanf_s.c",
  "src/vswprintf_s.c",
  "src/wcscpy_s.c",
  "src/vfwscanf_s.c",
  "src/memset_s.c",
  "src/wscanf_s.c",
  "src/vwscanf_s.c",
  "src/strtok_s.c",
  "src/wcsncpy_s.c",
  "src/vfscanf_s.c",
  "src/vsscanf_s.c",
  "src/wcstok_s.c",
  "src/securecutil.c",
  "src/gets_s.c",
  "src/swprintf_s.c",
  "src/strcpy_s.c",
  "src/wcscat_s.c",
  "src/strncpy_s.c",
]

include_common = [
  "include",
  "src",
]

ohos_static_library("libHWSecureC") {
  sources = src_libHWSecureC
  include_dirs = include_common
}

ohos_shared_library("libboundscheck") {
  sources = src_libHWSecureC
  include_dirs = include_common
}
```

```
# 放入 $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/paho.mqtt.c/BUILD.gn
#
# Copyright (c) [2020] Huawei Technologies Co.,Ltd.All rights reserved.
#
# OpenArkCompiler is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#
#     http://license.coscl.org.cn/MulanPSL
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
# FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v1 for more details.
#

import("//build/ohos.gni")
MQTT_SRC_DIR = "src"

MQTT_AS_SOURCE_FILES = [
    "${MQTT_SRC_DIR}/Base64.c",
    "${MQTT_SRC_DIR}/Clients.c",
    "${MQTT_SRC_DIR}/Heap.c",
    "${MQTT_SRC_DIR}/LinkedList.c",
    "${MQTT_SRC_DIR}/Log.c",
    "${MQTT_SRC_DIR}/Messages.c",
    "${MQTT_SRC_DIR}/MQTTAsync.c",
    "${MQTT_SRC_DIR}/MQTTAsyncUtils.c",
    "${MQTT_SRC_DIR}/MQTTPacket.c",
    "${MQTT_SRC_DIR}/MQTTPacketOut.c",
    "${MQTT_SRC_DIR}/MQTTPersistence.c",
    "${MQTT_SRC_DIR}/MQTTPersistenceDefault.c",
    "${MQTT_SRC_DIR}/MQTTProperties.c",
    "${MQTT_SRC_DIR}/MQTTProtocolClient.c",
    "${MQTT_SRC_DIR}/MQTTProtocolOut.c",
    "${MQTT_SRC_DIR}/MQTTReasonCodes.c",
    "${MQTT_SRC_DIR}/MQTTTime.c",
    "${MQTT_SRC_DIR}/OsWrapper.c",
    "${MQTT_SRC_DIR}/Proxy.c",
    "${MQTT_SRC_DIR}/SHA1.c",
    "${MQTT_SRC_DIR}/Socket.c",
    "${MQTT_SRC_DIR}/SocketBuffer.c",
    "${MQTT_SRC_DIR}/SSLSocket.c",
    "${MQTT_SRC_DIR}/StackTrace.c",
    "${MQTT_SRC_DIR}/Thread.c",
    "${MQTT_SRC_DIR}/Tree.c",
    "${MQTT_SRC_DIR}/utf-8.c",
    "${MQTT_SRC_DIR}/WebSocket.c",
]

ohos_shared_library("libpaho-mqtt3as") {
  sources = MQTT_AS_SOURCE_FILES
  include_dirs = ["./src", "./build"]
  deps=[
    "//third_party/openssl:libcrypto_static",
    "//third_party/openssl:ssl_source"
  ]
    
  cflags = ["-w", "-fPIC", "-Os", "-fvisibility=hidden", "-DOPENSSL", "-D_GNU_SOURCE", "-DPAHO_MQTT_EXPORTS=1"]
  libs= ["pthread", "dl"]
  ldflags = ["-Wl,-init,MQTTAsync_init"]
}
```

```gn
# 放入 $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/libssh/BUILD.gn
#
# Copyright (c) [2020] Huawei Technologies Co.,Ltd.All rights reserved.
#
# OpenArkCompiler is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#
#     http://license.coscl.org.cn/MulanPSL
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
# FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v1 for more details.
#

import("//build/ohos.gni")

src_libHWSecureC = [
"src/agent.c",
"src/auth.c",
"src/base64.c",
"src/bignum.c",
"src/buffer.c",
"src/callbacks.c",
"src/channels.c",
"src/client.c",
"src/config.c",
"src/connect.c",
"src/connector.c",
"src/curve25519.c",
"src/dh.c",
"src/ecdh.c",
"src/error.c",
"src/getpass.c",
"src/init.c",
"src/kdf.c",
"src/kex.c",
"src/known_hosts.c",
"src/knownhosts.c",
"src/legacy.c",
"src/log.c",
"src/match.c",
"src/messages.c",
"src/misc.c",
"src/options.c",
"src/packet.c",
"src/packet_cb.c",
"src/packet_crypt.c",
"src/pcap.c",
"src/pki.c",
"src/pki_container_openssh.c",
"src/poll.c",
"src/session.c",
"src/scp.c",
"src/socket.c",
"src/string.c",
"src/threads.c",
"src/wrapper.c",
"src/external/bcrypt_pbkdf.c",
"src/external/blowfish.c",
"src/external/chacha.c",
"src/external/poly1305.c",
"src/chachapoly.c",
"src/config_parser.c",
"src/token.c",
"src/pki_ed25519_common.c",
"src/threads/noop.c",
"src/threads/pthread.c",
"src/threads/libcrypto.c",
"src/pki_crypto.c",
"src/ecdh_crypto.c",
"src/libcrypto.c",
"src/dh_crypto.c",
"src/pki_ed25519.c",
"src/external/ed25519.c",
"src/external/fe25519.c",
"src/external/ge25519.c",
"src/external/sc25519.c",
"src/sftp.c",
"src/sftpserver.c",
"src/server.c",
"src/bind.c",
"src/bind_config.c",
"src/dh-gex.c",
"src/external/curve25519_ref.c",
]

include_common = [
  "./build",
  "./include/",
  "//third_party/openssl/include",
  "./include/libssh",
  "./build/include",
  "./build/src",
]

ohos_shared_library("libssh") {
  sources = src_libHWSecureC
  include_dirs = include_common
  deps=[
    "//third_party/openssl:libcrypto_static",
    "//third_party/openssl:ssl_source"
  ]
  cflags = ["-w", "-std=gnu99", "-fPIC", "-DLIBSSH_EXPORTS", "-D_GNU_SOURCE"]
}
```

```
# 放入 $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/nopoll/BUILD.gn
#
# Copyright (c) [2020] Huawei Technologies Co.,Ltd.All rights reserved.
#
# OpenArkCompiler is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#
#     http://license.coscl.org.cn/MulanPSL
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
# FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v1 for more details.
#
import("//build/ohos.gni")

src_nopoll = [
  "src/nopoll.c",
  "src/nopoll_conn.c",
  "src/nopoll_conn_opts.c",
  "src/nopoll_ctx.c",
  "src/nopoll_decl.c",
  "src/nopoll_io.c",
  "src/nopoll_listener.c",
  "src/nopoll_log.c",
  "src/nopoll_loop.c",
  "src/nopoll_msg.c",
  "src/nopoll_win32.c",
]

include_common = [
  "include",
  "src",
]

ohos_shared_library("libnopoll") {
  sources = src_nopoll
  include_dirs = include_common
  deps=[
    "//third_party/openssl:libcrypto_static",
    "//third_party/openssl:ssl_source"
  ]
  cflags = ["-w"]
}
```

由于libssh需要cmake生成一些包含各种配置的头文件，所以此处先用cmake命令生成中间文件。
> 此处需要根据您的开发板支持的功能向cmake传入参数，以便开启或关闭对应特性，如示例中的 `-DHAVED_GLOB=0`

命令如下：

``` bash
cd $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/libssh \
 && mkdir -p build \
 && cd build \
 && cmake .. \
     -DCMAKE_BUILD_TYPE=Release -DWITH_ZLIB=OFF \
     -DOPENSSL_ROOT_DIR=$MY_OHOS_DIR/third_party/openssl \
     -DOPENSSL_INCLUDE_DIR=$MY_OHOS_DIR/third_party/openssl/include \
     -DOPENSSL_CRYPTO_LIBRARY=$MY_OHOS_DIR/out/rk3568/packages/phone/system/lib/libcrypto.z.so \
     -DHAVE_STRTOULL=1 -DUNIX=1 -DHAVE_POLL=0 -DHAVE_GLOB=0 \
     -DHAVE_OPENSSL_CRYPTO_CTR128_ENCRYPT=1
```

paho.mqtt.c需要用make生成`VersionInfo.h`，执行如下命令生成：

```bash
cd $MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/paho.mqtt.c
make build/VersionInfo.h
```

nopoll需要对应的`$MY_OHOS_DIR/third_party/iot_device_sdk_c/third_party/nopoll/src/nopoll_config.h`:

```c
/*
 * Nopoll Library nopoll_config.h
 * Platform dependant definitions.
 *
 * This is a generated file.  Please modify 'configure.in'
 */

#ifndef __NOPOLL_CONFIG_H__
#define __NOPOLL_CONFIG_H__

/**
 * \addtogroup nopoll_decl_module
 * @{
 */

/**
 * @brief Allows to convert integer value (including constant values)
 * into a pointer representation.
 *
 * Use the oposite function to restore the value from a pointer to a
 * integer: \ref PTR_TO_INT.
 *
 * @param integer The integer value to cast to pointer.
 *
 * @return A \ref noPollPtr reference.
 */
#ifndef INT_TO_PTR
#define INT_TO_PTR(integer)   ((noPollPtr) (long) ((int)integer))
#endif

/**
 * @brief Allows to convert a pointer reference (\ref noPollPtr),
 * which stores an integer that was stored using \ref INT_TO_PTR.
 *
 * Use the oposite function to restore the pointer value stored in the
 * integer value.
 *
 * @param ptr The pointer to cast to a integer value.
 *
 * @return A int value.
 */
#ifndef PTR_TO_INT
#define PTR_TO_INT(ptr) ((int) (long) (ptr))
#endif

/**
 * @brief Allows to get current platform configuration. This is used
 * by Nopoll library but could be used by applications built on top of
 * Nopoll to change its configuration based on the platform information.
 */
#define NOPOLL_OS_UNIX (1)

/**
 * @internal Allows to now if the platform support vasprintf
 * function. Do not use this macro as it is supposed to be for
 * internal use.
 */
#define NOPOLL_HAVE_VASPRINTF (1)

/**
 * @brief Indicates that this platform have support for 64bits.
 */
#define NOPOLL_64BIT_PLATFORM (1)

/**
 * @brief Indicates where we have support for TLSv1.0 support.
 */
#define NOPOLL_HAVE_TLSv10_ENABLED (1)

/**
 * @brief Indicates where we have support for TLSv1.1 support.
 */
#define NOPOLL_HAVE_TLSv11_ENABLED (1)

/**
 * @brief Indicates where we have support for TLSv1.2 support.
 */
#define NOPOLL_HAVE_TLSv12_ENABLED (1)

/**
 * @brief Indicates where we have support for TLS flexible method where the highest TLS version will be negotiated.
 */
#define NOPOLL_HAVE_TLS_FLEXIBLE_ENABLED (1)

/* @} */

#endif
```

最后修改文件`$MY_OHOS_DIR/third_party/iot_device_sdk_c/BUILD.gn`，在`executable("mqtt_device_demo")`的`deps`
中加入以下依赖:

```gn
"third_party/huawei_secure_c:libboundscheck",
"third_party/paho.mqtt.c:libpaho-mqtt3as",
"third_party/libssh:libssh",
"third_party/nopoll:libnopoll",
```

结果如图所示：
![](doc/doc_cn/ohos_build/modify_main_gn.png)

<h2 id="2.2">2.2 其它三方库依赖</h2>

`libz.so`和`libssl.so`对应的源码在拉取OpenHarmony时会自获取，其分别对应`$MY_OHOS_DIR/third_party/libz`
，则属于`$MY_OHOS_DIR/third_party/openssl`。 这些库在编译`libboundscheck.so`等动态库时会以依赖的方式来驱使其编译，所以您只需要将对应的产物拷贝到目标设备中。

部分产物位于`$MY_OHOS_DIR/out/{产品型号}/common/common/`目录下，部分则位于特殊的位置。这些动态库通常`.z.so`
结尾。以rk3568为例，其`libssh.so`和`libz.so`分别处于：

```bash
$MY_OHOS_DIR/out/rk3568/common/common/libz.z.so
$MY_OHOS_DIR/out/rk3568/packages/phone/system/lib/libssl.z.so
```

对于示例开发板`rk3861`，其内部已经存在这些动态库，所以示例中无需拷贝这些库。如果您的目标设备缺少这些库，可以使用以下命令来查找动态库位置：

```bash
find $MY_OHOS_DIR/out -name {动态库名称}.z.so
find $MY_OHOS_DIR/out -name {动态库名称}.so
```

<h1 id="3">3 配置华为云接入参数并编译主程序</h1>

打开`$MY_OHOS_DIR/third_party/iot_device_sdk_c/src/device_demo/device_demo.c`，修改接入地址，设备id和设备密钥，如下图所示：
![](doc/doc_cn/ohos_build/modify_access_parameters.png)

最后执行编译：

```bash
cd $MY_OHOS_DIR
./build.sh --product-name 您使用的产品产品 --build-target iot_device_sdk_c 
```

输出如下内容则表示编译成功；
![](doc/doc_cn/ohos_build/successful_compilation.png)

<h1 id="4">4 验证对接华为云</h1>

将需要的动态库和主程序`./MQTT_Demo`拷贝到目标设备的同一路径下，其中动态库位于源码目录：`$MY_OHOS_DIR/ out/rk3568/common/common`的根目录下的.so文件，可执行文件MQTT_Demo位于源码目录：行`$MY_OHOS_DIR/out/rk3568/MQTT_Demo `，执行`export LD_LIBRARY_PATH=. && ./MQTT_Demo`，（如果无执行权限，可以通过chmod 777 MQTT_demo配置）出现`MqttBase_OnConnectSuccess`则表示成功连接华为云：
![](doc/doc_cn/ohos_build/execution_ouput.png)